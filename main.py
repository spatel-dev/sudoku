import base as B
import solver as SL
import advance_solver as A_SL
import test_utils as TU

grid = B.Grid()
#grid.verbose = True

TU.init_grid_hard3(grid)

SL.fill_obvious(grid)
# grid.pretty_print_grid(value=True,fill_status=True,poss=True,guess_status=True)

# IF we reach this point, WE NEED TO GUESS!

A_SL.capture_snapshot(grid)

while(True):
    print ("Fill count = %d " % grid.fill_count)
    status = SL.check_final_status(grid)
    if status:
        print ("Status till here is valid")
        A_SL.guess_one_element(grid)
        SL.fill_obvious(grid)
    else:
        print ("...Revert Snapshot...")
        grid.revert_snapshot()

grid.pretty_print_grid()
