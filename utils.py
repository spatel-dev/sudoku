# Utility functions

def check_full_validity(values, size, verbose):
    # values     : list; all values to be tested
    # size       : integer; number of unique elements, should also be max
    # return     : boolean; True if valid, else False
    # description:
    #   Checks if values are between 1 and size, including.
    #   Values must also be unique.
    valid = True
    num_unique_elements = len(set(values))
    if num_unique_elements != size:
        valid = False
    if min(values) != 1:
        valid = False
    if max(values) != size:
        valid = False
    if verbose:
        if valid:
            print ("DEBUG: Full Validity Passed")
        else:
            print ("DEUBG: Full Validity Failed")
    return valid


def check_partial_validity(values, size, verbose):
    # values     : list; all values to be tested
    # size       : integer; number of unique elements
    # return     : boolean; True if valid, else False
    # description:
    #   Checks if values are between 0 and size, including
    #   All values, if not 0, must be unique
    valid = True
    while(0 in values):
        values.remove(0)
    if len(values) == 0:
        # All values 0. This is valid. End here.
        return True
    unique_size = len(values)
    num_unique_elements = len(set(values))
    if len(values) != len(set(values)):
        valid = False
    if max(values) > size:
        valid = False
    if verbose:
        if valid:
            print ("DEBUG: Partial Validity Passed")
        else:
            print ("DEBUG: Partial Validity Failed")
    return valid
