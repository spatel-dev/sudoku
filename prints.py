
def print_row(size, row_idx, values, boundaries):
    for col_idx in range(0,size):
        if col_idx in boundaries:
            print (" | ", end='')
        else:
            print (" ", end='')
        print (values[row_idx][col_idx], end='')
    print (" |")

def print_vline(size, boundaries):
    for idx in range(0,size):
        if idx in boundaries:
            print ("   ", end='')
        else:
            print (" ", end='')
        print ("-", end='')
    print (" ")

def print_grid(grid_values, grid_boundaries, grid_size):
    for idx in range(0,grid_size):
        if idx in grid_boundaries:
            print_vline(grid_size, grid_boundaries)
        else:
            print ("", end='')
        print_row(grid_size, idx, grid_values, grid_boundaries)
    print_vline(grid_size, grid_boundaries)

