import utils
import prints
import copy

class Grid():
    def __init__(self):
        # verbose: Sets the verbosity for debug
        self.verbose = False
        # init grid with size 9
        self.value = {}
        self.filled = {}
        self.poss = {}
        self.guess_status = {}
        self.fill_count = 0

        # Row and Col helpers
        self.size = 9
        self.idx = range(0,self.size)
        self.init_grid_to_default()

        # TODO: Below hardcoded values can be derived from size
        self.boxes = 3
        self.boundary = [0,3,6]
        self.box_idx_map = {0:0,1:0,2:0,3:1,4:1,5:1,6:2,7:2,8:2}

        # Parameters for guessing algorithm
        self.curr_guess_level = 1
        self.snapshots = {}

    def init_grid_to_default(self):
        for row in self.idx:
            self.value[row] = {}
            self.filled[row] = {}
            self.poss[row] = {}
            self.guess_status[row] = {}
            for col in self.idx:
                self.value[row][col] = 0 # init with value 0
                self.filled[row][col] = False
                self.poss[row][col] = list(range(1,self.size+1))
                self.guess_status[row][col] = 0

    def pretty_print_grid(self, value=True, fill_status=False, poss=False, guess_status=False):
        if value:
            print ("::::VALUES::::")
            prints.print_grid(self.value, self.boundary, self.size)
        if fill_status:
            print ("::::FILL STATUS::::")
            prints.print_grid(self.filled, self.boundary, self.size)
        if poss:
            print ("::::POSS::::")
            prints.print_grid(self.poss, self.boundary, self.size)
        if guess_status:
            print ("::::GUESS STATUS::::")
            prints.print_grid(self.guess_status, self.boundary, self.size)

    def take_snapshot(self):
        new_snap_idx = len(self.snapshots)
        self.snapshots[new_snap_idx] = {\
                'value':copy.deepcopy(self.value),\
                'poss':copy.deepcopy(self.poss),\
                'filled':copy.deepcopy(self.filled),\
                'guess_status':copy.deepcopy(self.guess_status),\
                'curr_guess_level':self.curr_guess_level,\
                'fill_count':self.fill_count}

    def revert_snapshot(self):
        if len(self.snapshots) == 0:
            raise Exception("Reverting a snapshot but none available")
        snap_idx = len(self.snapshots) - 1
        self.value = copy.deepcopy(self.snapshots[snap_idx]['value'])
        self.poss = copy.deepcopy(self.snapshots[snap_idx]['poss'])
        self.filled = copy.deepcopy(self.snapshots[snap_idx]['filled'])
        self.guess_status = copy.deepcopy(self.snapshots[snap_idx]['guess_status'])
        self.curr_guess_level = self.snapshots[snap_idx]['curr_guess_level']
        self.fill_count = self.snapshots[snap_idx]['fill_count']
        # del(self.snapshots[snap_idx])

    def add_element(self,row,col,value):
        if (self.value[row][col] == 0):
            self.value[row][col] = value
            self.poss[row][col] = []
            self.filled[row][col] = True
            self.guess_status[row][col] = self.curr_guess_level
            self.fill_count += 1
            if self.verbose:
                print ("DEBUG: Adding value %d to row %d and col %d" % (value,row,col))
        else:
            raise Exception("Adding value %d to row %d, col %d which already has value %d"\
                    % (value, row, col, self.value[row][col])\
                    )

    def get_boxes(self):
        boxes = {}
        for box_row_idx in range(0,self.boxes):
            boxes[box_row_idx] = {}
            for box_col_idx in range(0,self.boxes):
                boxes[box_row_idx][box_col_idx] = []
        for row_idx in self.idx:
            for col_idx in self.idx:
                box_row_idx = self.box_idx_map[row_idx]
                box_col_idx = self.box_idx_map[col_idx]
                boxes[box_row_idx][box_col_idx].append(self.value[row_idx][col_idx])
        return boxes

    def check_boxes_valid(self, partial=False):
        boxes_valid = True
        boxes = self.get_boxes()
        for box_row_idx in boxes:
            for box_col_idx in boxes:
                elements = self.get_box_elements(box_row_idx,box_col_idx)
                if self.verbose:
                    print ("DEBUG: Checking Box Validity")
                    print ("DEBUG: Elements: %s" % elements)
                if partial:
                    if not utils.check_partial_validity(elements, self.size, self.verbose):
                        boxes_valid = False
                else:
                    if not utils.check_full_validity(elements, self.size, self.verbose):
                        boxes_valid = False
        return boxes_valid

    def check_rows_valid(self, partial=False):
        rows_valid = True
        for row_idx in self.idx:
            elements = self.get_row_elements(row_idx)
            if self.verbose:
                print ("DEBUG: Checking Row Validity")
                print ("DEBUG: Elements: %s" % elements)
            if partial:
                row_valid = utils.check_partial_validity(elements, self.size, self.verbose)
            else:
                row_valid = utils.check_full_validity(elements, self.size, self.verbose)
            if not row_valid:
                rows_valid = False
        return rows_valid

    def check_cols_valid(self, partial=False):
        cols_valid = True
        for col_idx in self.idx:
            elements = self.get_col_elements(col_idx)
            if self.verbose:
                print ("DEBUG: Checking Col Validity")
                print ("DEBUG: Elements: %s" % elements)
            if partial:
                col_valid = utils.check_partial_validity(elements, self.size, self.verbose)
            else:
                col_valid = utils.check_full_validity(elements, self.size, self.verbose)
            if not col_valid:
                cols_valid = False
        return cols_valid

    def check_poss_valid(self):
        # Checks if all non-filled entries have
        # atleast 1 possibility
        if self.verbose:
            print ("DEBUG: Checking Poss Validity")
        poss_valid = True
        for row in self.idx:
            for col in self.idx:
                if not self.filled[row][col]:
                    if len(self.poss[row][col]) == 0:
                        poss_valid = False
                        if self.verbose:
                            print ("DEBUG: Poss Check Failed.")
                            print ("DEBUG: Row %d, Col %d, Poss: %s" % (row,col,self.poss[row][col]))
        return poss_valid

    def check_strict_validity(self):
        all_valid = True
        if not self.check_boxes_valid():
            all_valid = False
        if not self.check_rows_valid():
            all_valid = False
        if not self.check_cols_valid():
            all_valid = False
        if not self.check_poss_valid():
            all_valid = False
        return all_valid

    def check_easy_validity(self, fatal=False):
        all_valid = True
        if not self.check_boxes_valid(partial=True):
            if self.verbose:
                print ("DEBUG: Boxes validity failed")
            all_valid = False
        if not self.check_rows_valid(partial=True):
            if self.verbose:
                print("DEBUG: Row validity failed")
            all_valid = False
        if not self.check_cols_valid(partial=True):
            if self.verbose:
                print("DEBUG: Col validity failed")
            all_valid = False
        if not self.check_poss_valid():
            if self.verbose:
                print("DEBUG: Poss validity failed")
            all_valid = False
        if fatal:
            if not all_valid:
                raise Exception("FATAL partial validity check failed!")
        return all_valid

    def get_row_elements(self,row):
        return list(self.value[row].values())

    def get_col_elements(self,col):
        col_elements = []
        for row in self.idx:
            col_elements.append(self.value[row][col])
        return col_elements

    def get_box_elements(self,row,col):
        box_row_idx = self.box_idx_map[row]
        box_col_idx = self.box_idx_map[col]
        boxes = self.get_boxes()
        return boxes[box_row_idx][box_col_idx]

