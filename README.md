# sudoku

Sudoku Solver

File Hierarchy:

 - main.py is the top level file.
 - solver.py has algorithm to easy fill a sudoku (no guessing).
   This is imported by main.py.
 - advance_solver.py has algorithm to guess and fill sudoku.
   This is imported by main.py.
 - base.py has the class definition for grid and basic functionality.
 - print.py has functions to print the grid.
 - utils.py has utility functions, checking validity of the grid.
 - test_utils.py has functions to initialize a sudoku grid for testing.

Current Status:
 - Able to solve most of the sudokus
 - Clean up prints
