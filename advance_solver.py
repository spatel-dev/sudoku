import random

def get_random(rmax, rmin=0, step=1):
    return random.randrange(rmin, rmax, step)

def get_random_from_list(all_poss):
    return random.choice(all_poss)

def get_min_poss_length(grid):
    all_poss = []
    for row in grid.idx:
        for col in grid.idx:
            if not grid.filled[row][col]:
                all_poss.append(len(grid.poss[row][col]))
    if len(all_poss) == 0:
        grid.pretty_print_grid(value=True,fill_status=True,poss=True,guess_status=True)
    return min(all_poss)

def get_entries_with_posslen(grid, posslen):
    entries = []
    for row in grid.idx:
        for col in grid.idx:
            if len(grid.poss[row][col]) == posslen:
                entries.append((row,col))
    return entries

def get_entry_to_guess(grid):
    min_posslen = get_min_poss_length(grid)
    min_posslen_entries = get_entries_with_posslen(grid, min_posslen)
    random_entry_idx = get_random(len(min_posslen_entries))
    return min_posslen_entries[random_entry_idx]

def guess_entry_value(grid,row,col):
    entry_poss = grid.poss[row][col]
    guess_value = get_random_from_list(entry_poss)
    grid.add_element(row,col,guess_value)
    print ("Guess row:%d col:%d to value %d" %(row,col,guess_value))

def guess_one_element(grid):
    (row,col) = get_entry_to_guess(grid)
    guess_entry_value(grid,row,col)

def capture_snapshot(grid):
    grid.take_snapshot()
    grid.curr_guess_level += 1
