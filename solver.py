
def trim_cell_poss(grid, row, col):
    progress = False
    row_elements = grid.get_row_elements(row)
    col_elements = grid.get_col_elements(col)
    box_elements = grid.get_box_elements(row,col)
    all_elements = row_elements + col_elements + box_elements
    unique_elements = list(set(all_elements))
    for element in unique_elements:
        if element in grid.poss[row][col]:
            grid.poss[row][col].remove(element)
            progress = True
    return progress

def trim_all_poss(grid):
    any_progress = False
    for row in grid.idx:
        for col in grid.idx:
            cell_progress = trim_cell_poss(grid, row, col)
            if cell_progress:
                any_progress = True
    return any_progress

def fill_from_poss(grid):
    for row in grid.idx:
        for col in grid.idx:
            if grid.value[row][col] == 0:
                if len(grid.poss[row][col]) == 1:
                    grid.add_element(row,col,grid.poss[row][col][0])

def fill_obvious(grid):
    grid.check_easy_validity(fatal=False)
    while(trim_all_poss(grid)):
        fill_from_poss(grid)
    grid.check_easy_validity(fatal=False)

def check_final_status(grid):
    if grid.fill_count == 81:
        if grid.check_strict_validity():
            print ("Sudoku solved...")
            grid.pretty_print_grid()
            exit()
        else:
            return False
    else:
        if grid.check_easy_validity():
            return True
        else:
            return False
